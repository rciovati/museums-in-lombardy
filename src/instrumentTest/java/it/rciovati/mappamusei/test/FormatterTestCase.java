package it.rciovati.mappamusei.test;

import it.rciovati.mappamusei.util.Formatter;
import junit.framework.TestCase;

public class FormatterTestCase extends TestCase {

	public void testFirstFormat() {

		String city = "Legnano";
		String province = "Milano";
		String expected = "Legnano (Milano)";

		assertEquals(expected,
				Formatter.formatCityAndProvince(city, province));

	}
	
	public void testSecondFormat() {

		String city = "Milano";
		String province = "Milano";
		String expected = "Milano";

		assertEquals(expected,
				Formatter.formatCityAndProvince(city, province));

	}
}
