package it.rciovati.mappamusei.test;

import java.util.ArrayList;

import it.rciovati.mappamusei.provider.MuseumContract;
import it.rciovati.mappamusei.provider.MuseumsProvider;
import it.rciovati.mappamusei.util.MathUtil;
import it.rciovati.mappamusei.util.UriUtils;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.RemoteException;
import android.test.ProviderTestCase2;

public class MuseumContentProviderTestCase extends
		ProviderTestCase2<MuseumsProvider> {

	public MuseumContentProviderTestCase() {
		super(MuseumsProvider.class, MuseumsProvider.AUTHORITY);
	}

	public void testInsert() {

		ContentResolver cr = getMockContentResolver();

		ContentValues cv = createContentValues("Prova", 12.3, 24.5);

		cr.insert(MuseumContract.CONTENT_URI, cv);

		Cursor c = cr.query(MuseumContract.CONTENT_URI,
				new String[] { MuseumContract._ID }, null, null, null);

		assertEquals(1, c.getColumnCount());
		assertEquals(1, c.getCount());

	}

	public void testInsertWithOperations() {

		ContentResolver cr = getMockContentResolver();

		ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>(
				3);

		ContentProviderOperation.Builder operation = ContentProviderOperation
				.newInsert(MuseumContract.CONTENT_URI);

		operations.add(operation.withValues(
				createContentValues("Prova1", 12.3, 32.5)).build());

		operations.add(operation.withValues(
				createContentValues("Prova2", 15.3, 22.5)).build());

		operations.add(operation.withValues(
				createContentValues("Prova3", 16.3, 672.5)).build());

		try {
			cr.applyBatch(MuseumsProvider.AUTHORITY, operations);
		} catch (RemoteException e) {
			fail(e.getMessage());
		} catch (OperationApplicationException e) {
			fail(e.getMessage());
		}

		Cursor c = cr.query(MuseumContract.CONTENT_URI,
				new String[] { MuseumContract._ID }, null, null, null);

		assertEquals(1, c.getColumnCount());
		assertEquals(3, c.getCount());

	}

	public void testDistanceIsProperlyCalculated() {

		ContentResolver cr = getMockContentResolver();

		double startLat = 45.4897;
		double startLon = 9.191;

		double endLat = 45.463743;
		double endLon = 9.183025;

		ContentValues cv = createContentValues("Milano", startLat, startLon);

		cr.insert(MuseumContract.CONTENT_URI, cv);

		Uri u = UriUtils.buildMuseumsContentUriWithUserCoordinates(endLat,
				endLon);

		Cursor c = cr.query(u, new String[] { MuseumContract._ID }, null, null,
				null);

		float[] results = new float[2];

		Location.distanceBetween(startLat, startLon, endLat, endLon, results);

		assertEquals(2, c.getColumnCount());

		c.moveToFirst();

		double fromDb = convertPartialDistanceToMeters(c.getDouble(1));

		assertTrue(results[0] - 5 < fromDb && fromDb <= results[0] + 5);

	}

	private static double convertPartialDistanceToMeters(double result) {
		return Math.acos(result) * 6371000;
	}

	private ContentValues createContentValues(String name, double lat,
			double lon) {
		ContentValues cv = new ContentValues();
		cv.put(MuseumContract.NAME, name);
		cv.put(MuseumContract.LATITUDE, lat);
		cv.put(MuseumContract.LONGITUDE, lon);
		cv.put(MuseumContract.COSLAT, Math.cos(MathUtil.deg2rad(lat)));
		cv.put(MuseumContract.SINLAT, Math.sin(MathUtil.deg2rad(lat)));
		cv.put(MuseumContract.COSLNG, Math.cos(MathUtil.deg2rad(lon)));
		cv.put(MuseumContract.SINLNG, Math.sin(MathUtil.deg2rad(lon)));
		return cv;
	}

}
