package it.rciovati.mappamusei.test;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;

import it.rciovati.mappamusei.util.DataCleaner;
import junit.framework.TestCase;

@SuppressWarnings("unused")
public class DataCleanerTestCase extends TestCase {

	public void testExtractEmail() {

		String dirty = "rciovati@gmail.com kjahsdkjahdkjaskj";
		String cleaned = "rciovati@gmail.com";

		assertEquals(cleaned, DataCleaner.extractEmail(dirty));

	}

	public void testExtractEmail2() {

		String dirty = "rciovati@gmail.com annalisa.berti@gmail.com";
		String cleaned = "rciovati@gmail.com";

		assertEquals(cleaned, DataCleaner.extractEmail(dirty));

	}

	public void testExtractEmail3() {

		String dirty = "rciovati@gmail.com oppure annalisa.berti@gmail.com";
		String cleaned = "rciovati@gmail.com";

		assertEquals(cleaned, DataCleaner.extractEmail(dirty));

	}

	public void testExtractWebsite() {

		String dirty = "www.comune.mantova.it, www.mumm.mantova.it";
		String cleaned = "www.comune.mantova.it";

		assertEquals(cleaned, DataCleaner.extractWebSite(dirty));

	}

	public void testExtractWebsite2() {

		String dirty = "www.valcavargna.com - www.cmalpilepontine.it";
		String cleaned = "www.valcavargna.com";

		assertEquals(cleaned, DataCleaner.extractWebSite(dirty));

	}

	public void testExtractWebsite3() {

		String dirty = "www.bergamoestoria.it www.bergamoestoria.org";
		String cleaned = "www.bergamoestoria.it";

		assertEquals(cleaned, DataCleaner.extractWebSite(dirty));

	}

	public void testExtractWebsite4() {

		String dirty = "http://comune.malnate.va.it";
		String cleaned = "http://comune.malnate.va.it";

		assertEquals(cleaned, DataCleaner.extractWebSite(dirty));

	}

	public void testExtractPhoneNumber() {

		String dirty = "0344- 63162, 031306205";
		String cleaned = normalizePhoneNumber("034463162");

		assertEquals(cleaned, DataCleaner.extractPhoneNumber(dirty));

	}

	public void testExtractPhoneNumber2() {

		String dirty = "031 965885";
		String cleaned = normalizePhoneNumber("031 965885");

		assertEquals(cleaned, DataCleaner.extractPhoneNumber(dirty));

	}

	public void testExtractPhoneNumber3() {

		String dirty = "0344-32115 (Comune)";
		String cleaned = normalizePhoneNumber("0344-32115");

		assertEquals(cleaned, DataCleaner.extractPhoneNumber(dirty));

	}

	// public void testExtractPhoneNumber4() {
	//
	// String dirty = "0375 205344 0375 284428";
	// String cleaned = normalizePhoneNumber("0375 205344");
	//
	// assertEquals(cleaned, DataCleaner.extractPhoneNumber(dirty));
	//
	// }
	//
	// public void testExtractPhoneNumber5() {
	//
	// String dirty = "0376/806330-030/392652";
	// String cleaned = normalizePhoneNumber("0376806330");
	//
	// assertEquals(cleaned, DataCleaner.extractPhoneNumber(dirty));
	//
	// }

	public void testExtractPhoneNumber6() {

		String dirty = "0332.255476 (reception: 0332820409)";
		String cleaned = normalizePhoneNumber("0332255476");

		assertEquals(cleaned, DataCleaner.extractPhoneNumber(dirty));

	}

	private String normalizePhoneNumber(String number) {
		try {
			return PhoneNumberUtil.getInstance().format(
					PhoneNumberUtil.getInstance().parseAndKeepRawInput(number,
							"IT"), PhoneNumberFormat.INTERNATIONAL);
		} catch (NumberParseException e) {
			return "";
		}
	}

}
