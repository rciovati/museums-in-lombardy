/*
 * Copyright 2012 Riccardo Ciovati
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package it.rciovati.mappamusei.service;

import android.app.IntentService;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Intent;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;

import org.apache.commons.lang.WordUtils;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import au.com.bytecode.opencsv.CSVReader;
import it.rciovati.mappamusei.BuildConfig;
import it.rciovati.mappamusei.provider.MuseumContract;
import it.rciovati.mappamusei.provider.MuseumsProvider;
import it.rciovati.mappamusei.util.DataCleaner;
import it.rciovati.mappamusei.util.MathUtil;

public class DataFetcherService extends IntentService {

    public static final String EXTRA_RESULT_RECEIVER = "result_receiver";

    public static final int RESULT_DATA_FETCHED = 1;

    public static final int RESULT_NETWORK_ERROR = 2;

    private static final String LOG_TAG = DataFetcherService.class.getSimpleName();

    private ResultReceiver mResultReceiver;

    private DataProvider mDataProvider;

    long initTime;

    public DataFetcherService() {
        super("DataFetcherService");
        mDataProvider = new DataProviderImpl();
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        initTime = System.nanoTime();

        mResultReceiver = intent.getParcelableExtra(EXTRA_RESULT_RECEIVER);

        try {

            InputStream inputStream = mDataProvider.obtainData();

            if (BuildConfig.DEBUG) {
                Log.d(LOG_TAG, "obtained");
            }

            parseStream(inputStream);

        } catch (Exception e) {
            mResultReceiver.send(RESULT_NETWORK_ERROR, null);
            Log.e(LOG_TAG, "err", e);
        } finally {
            mDataProvider.close();
        }

        if (BuildConfig.DEBUG) {
            Log.d(LOG_TAG, "done in: " + ((System.nanoTime() - initTime) / 1000000000) + " secs");
        }

    }

    public void setDataProvider(DataProvider dp) {
        mDataProvider = dp;
    }

    private void parseStream(InputStream in) throws Exception {
        CSVReader reader = new CSVReader(new InputStreamReader(in));
        String[] nextLine;

        ContentValues cv = new ContentValues();

        ArrayList<ContentProviderOperation> operations = new ArrayList<>(
                230);

        if (BuildConfig.DEBUG) {
            Log.d(LOG_TAG, "parsing");
        }

        final String YES = "SI";
        final String PARTIALLY = "ACCESSO PARZIALE";
        final String TOTALLY = "ACCESSO TOTALE";
        final String OPEN = "APERTA";
        final String FREE = "GRATUITO";

        char[] addressDelimiters = {
                '.', '"', '\'', ' ', '/', '-', ','
        };
        char[] nameDelimiters = {
                '.', '"', '\'', ' '
        };
        char[] cityDelimiters = {
                ' ', '-'
        };

        int rowNumber = 0;
        while ((nextLine = reader.readNext()) != null) {

            rowNumber++;

            // first line is just the header, skip it
            if (rowNumber == 1)
                continue;

            cv.clear();

            // parcheggi per disabili
            String disabledParks = nextLine[25];
            Log.d("disabledParks", disabledParks);
            if (YES.equals(disabledParks)) {
                cv.put(MuseumContract.PARKING_FOR_DISABLED, 1);
            } else {
                cv.put(MuseumContract.PARKING_FOR_DISABLED, 0);
            }

            // sede accessibile ai disabili
            String accessibleToDisabled = nextLine[42];
            int outValue = 0; // not accessible

            if (PARTIALLY.equals(accessibleToDisabled)) {
                outValue = 1;
            } else if (TOTALLY.equals(accessibleToDisabled)) {
                outValue = 2;
            }

            cv.put(MuseumContract.ACCESSIBLE_TO_DISABLED, outValue);

            cv.put(MuseumContract.NAME,
                    WordUtils.capitalize(nextLine[4].toLowerCase(), nameDelimiters));

            cv.put(MuseumContract.LOCATION_NAME,
                    WordUtils.capitalize(nextLine[5].toLowerCase(), nameDelimiters));

            cv.put(MuseumContract.CATEGORY, WordUtils.capitalize(nextLine[15].toLowerCase()));

            cv.put(MuseumContract.CITY,
                    WordUtils.capitalize(nextLine[3].toLowerCase(), cityDelimiters));

            cv.put(MuseumContract.PROVINCE, WordUtils.capitalize(nextLine[2].toLowerCase()));

            cv.put(MuseumContract.ZIP, nextLine[8]);

            cv.put(MuseumContract.ADDRESS,
                    WordUtils.capitalize(nextLine[6].toLowerCase(), addressDelimiters));

            cv.put(MuseumContract.PHONE, DataCleaner.extractPhoneNumber(nextLine[9]));
            cv.put(MuseumContract.EMAIL, DataCleaner.extractEmail(nextLine[11]));

            cv.put(MuseumContract.WEBSITE, DataCleaner.extractWebSite(nextLine[12]));

            cv.put(MuseumContract.IS_OPEN, YES.equals(nextLine[17]) ? 1 : 0);

            cv.put(MuseumContract.FREE_ENTRY, FREE.equals(nextLine[43]) ? 1 : 0);

            cv.put(MuseumContract.OPENING_HOUR_DESCRIPTION, nextLine[21]);
            cv.put(MuseumContract.OPENING_HOUR_NOTES, nextLine[20]);

            cv.put(MuseumContract.PARKINGS, YES.equals(nextLine[24]) ? 1 : 0);

            cv.put(MuseumContract.LIBRARY, YES.equals(nextLine[63]) ? 1 : 0);
            cv.put(MuseumContract.EDUCATIONAL_ACTIVITIES, YES.equals(nextLine[62]) ? 1 : 0);
            cv.put(MuseumContract.STUDY_ROOM, 0);

            cv.put(MuseumContract.GUIDED_VISIT, 0);

            String latLon = nextLine[76];

            // this is in the form (45.8964306, 9.6545868), so we need to remove
            // the first and the last bracket and split by the comma.
            if (TextUtils.isEmpty(latLon)) {
                continue;
            }
            String[] latLonSplitted = latLon.substring(1, latLon.length() - 1).split(",");

            if (latLonSplitted.length == 2) {

                Double lat = Double.parseDouble(latLonSplitted[0]);
                Double lon = Double.parseDouble(latLonSplitted[1]);

                cv.put(MuseumContract.LATITUDE, lat);
                cv.put(MuseumContract.LONGITUDE, lon);
                // We also store the sin e cos of latitude and longitude
                cv.put(MuseumContract.COSLAT, Math.cos(MathUtil.deg2rad(lat)));
                cv.put(MuseumContract.SINLAT, Math.sin(MathUtil.deg2rad(lat)));
                cv.put(MuseumContract.COSLNG, Math.cos(MathUtil.deg2rad(lon)));
                cv.put(MuseumContract.SINLNG, Math.sin(MathUtil.deg2rad(lon)));
            }

            operations.add(ContentProviderOperation.newInsert(MuseumContract.CONTENT_URI)
                    .withValues(cv).build());

        }

        if (BuildConfig.DEBUG) {
            Log.d(LOG_TAG, "parsed");
        }

        //Delete all!!
        getContentResolver().delete(MuseumContract.CONTENT_URI, null, null);
        getContentResolver().applyBatch(MuseumsProvider.AUTHORITY, operations);

        reader.close();

        if (BuildConfig.DEBUG) {
            Log.d(LOG_TAG, "done");
        }

        mResultReceiver.send(RESULT_DATA_FETCHED, null);
    }
}
