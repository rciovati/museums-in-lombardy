/*
 * Copyright 2012 Riccardo Ciovati
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package it.rciovati.mappamusei.adapter;

import it.rciovati.mappamusei.provider.MuseumContract;
import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
//Unused ATM
public class SearchMuseumAdapter extends CursorAdapter {

    private LayoutInflater mLayoutInflater;

    private int mProviceIndex;

    private int mNameIndex;

    private int mCityIndex;

    public SearchMuseumAdapter(Context context, Cursor c) {
        super(context, c, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public Cursor swapCursor(Cursor cursor) {
        mProviceIndex = cursor.getColumnIndex(MuseumContract.PROVINCE);
        mNameIndex = cursor.getColumnIndex(MuseumContract.NAME);
        mCityIndex = cursor.getColumnIndex(MuseumContract.CITY);
        return super.swapCursor(cursor);
    }

    @Override
    public void bindView(View rowView, Context context, Cursor cursor) {
        ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.title.setText(cursor.getString(mNameIndex));

        StringBuilder sb = new StringBuilder();
        sb.append(cursor.getString(mCityIndex));
        sb.append(" (");
        sb.append(cursor.getString(mProviceIndex));
        sb.append(")");
        
        holder.subtitle.setText(sb.toString());
    }

    @Override
    public View newView(Context arg0, Cursor arg1, ViewGroup arg2) {
        ViewHolder holder = new ViewHolder();
        View rowView = mLayoutInflater.inflate(android.R.layout.simple_list_item_2, null);
        holder.title = (TextView) rowView.findViewById(android.R.id.text1);
        holder.subtitle = (TextView) rowView.findViewById(android.R.id.text2);
        rowView.setTag(holder);
        return rowView;
    }

    class ViewHolder {
        TextView title;

        TextView subtitle;
    }

}
