/*
 * Copyright 2012 Riccardo Ciovati
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package it.rciovati.mappamusei.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import it.rciovati.mappamusei.R;
import it.rciovati.mappamusei.provider.MuseumContract;
import it.rciovati.mappamusei.util.Formatter;


public class MuseumsAdapter extends CursorAdapter implements StickyListHeadersAdapter {

    public enum AdapterMode {
        SORTED_BY_CATEGORY, SORTED_BY_PROVINCE
    }

    class HeaderViewHolder {
        TextView text;
    }

    class ViewHolder {

        TextView city;

        TextView extra;

        ImageView free;

        TextView locationName;

        TextView name;
    }

    private AdapterMode mAdapterMode;

    protected int mAddressIndex;

    protected int mCategoryIndex;

    protected int mCityIndex;

    protected final Context mContext;

    private int mExtraIndex;

    private int mHeaderColumnIndex;

    private ForegroundColorSpan mHilightTextSpan;

    protected final LayoutInflater mInflater;

    protected int mIsFreeIndex;

    private int mLocationNameIndex;

    protected int mNameIndex;

    protected int mProviceIndex;

    private String mSearchQuery = "";

    private boolean mSetCursorCalled = false;

    private int mSearchQueryLenght;

    public MuseumsAdapter(Context context) {
        super(context, null, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mAdapterMode = AdapterMode.SORTED_BY_PROVINCE;
        mHilightTextSpan = new ForegroundColorSpan(context.getResources().getColor(
                R.color.search_highlight));
    }

    @Override
    public void bindView(View view, Context arg1, Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();

        String name = cursor.getString(mNameIndex);

        if (!TextUtils.isEmpty(mSearchQuery)) {
            int start = name.toLowerCase().indexOf(mSearchQuery);
            int length = mSearchQueryLenght;
            SpannableString sp = new SpannableString(name);
            sp.setSpan(mHilightTextSpan, start, start + length, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            holder.name.setText(sp, BufferType.SPANNABLE);
        } else {
            holder.name.setText(name);
        }

        String museumLocationName = cursor.getString(mLocationNameIndex);

        if (museumLocationName != null && museumLocationName.compareToIgnoreCase(name) != 0) {
            holder.locationName.setText(museumLocationName);
            holder.locationName.setVisibility(View.VISIBLE);
        } else {
            holder.locationName.setVisibility(View.GONE);
        }

        String city = cursor.getString(mCityIndex);

        if (mAdapterMode == AdapterMode.SORTED_BY_PROVINCE) {

            holder.city.setText(city);

            String extra = cursor.getString(mExtraIndex);

            SpannableStringBuilder builder = Formatter.formatListCategory(extra);

            holder.extra.setText(builder, BufferType.SPANNABLE);
        } else {
            String province = cursor.getString(mProviceIndex);

            String formattedCityAndProvince = Formatter.formatCityAndProvince(city, province);

            holder.city.setText(formattedCityAndProvince);

            holder.extra.setVisibility(View.GONE);
        }
    }

    @Override
    public View getHeaderView(int i, View view, ViewGroup viewGroup) {

        HeaderViewHolder holder;
        Cursor cursor = getCursor();
        cursor.moveToPosition(i);

        if (view == null) {
            holder = new HeaderViewHolder();
            view = mInflater.inflate(R.layout.sticky_list_item_header, null);
            holder.text = (TextView) view.findViewById(R.id.sectionHeader);
            view.setTag(holder);
        } else {
            holder = (HeaderViewHolder) view.getTag();
        }

        if (mAdapterMode == AdapterMode.SORTED_BY_PROVINCE) {
            String province = cursor.getString(mProviceIndex);
            holder.text.setText(mContext.getString(
                    R.string.province_of, province).toUpperCase());
        } else {
            String category = cursor.getString(mCategoryIndex);
            holder.text.setText(category.toUpperCase());
        }

        return view;
    }

    @Override
    public long getHeaderId(int i) {
        Cursor c = getCursor();
        c.moveToPosition(i);
        String province = c.getString(mHeaderColumnIndex);
        return province.hashCode();
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        ViewHolder holder = new ViewHolder();
        View v = mInflater.inflate(R.layout.museums_list_item, null);
        holder.name = (TextView) v.findViewById(R.id.museumName);
        holder.locationName = (TextView) v.findViewById(R.id.museumLocationName);
        holder.city = (TextView) v.findViewById(R.id.museumCity);
        holder.extra = (TextView) v.findViewById(R.id.museumExtra);
        v.setTag(holder);
        return v;
    }

    public void resetSearchQuery() {
        mSearchQuery = "";
    }

    public void setAdapterMode(AdapterMode mode) {
        mAdapterMode = mode;

        if (mode == AdapterMode.SORTED_BY_PROVINCE) {
            mHeaderColumnIndex = mProviceIndex;
            mExtraIndex = mCategoryIndex;
        } else {
            mHeaderColumnIndex = mCategoryIndex;
            mExtraIndex = mProviceIndex;
        }
    }

    public void setCursor(Cursor cursor, AdapterMode mode) {
        mSetCursorCalled = true;
        swapCursor(cursor);
        setAdapterMode(mode);
    }

    public void setSearchQuery(String searchQuery) {
        mSearchQuery = searchQuery.toLowerCase();
        mSearchQueryLenght = searchQuery.length();
    }

    @Override
    public Cursor swapCursor(Cursor cursor) {

        if (!mSetCursorCalled) {
            new RuntimeException("call setCursor method");
        }

        mProviceIndex = cursor.getColumnIndex(MuseumContract.PROVINCE);
        mNameIndex = cursor.getColumnIndex(MuseumContract.NAME);
        mCityIndex = cursor.getColumnIndex(MuseumContract.CITY);
        mIsFreeIndex = cursor.getColumnIndex(MuseumContract.FREE_ENTRY);
        mLocationNameIndex = cursor.getColumnIndex(MuseumContract.LOCATION_NAME);
        mCategoryIndex = cursor.getColumnIndex(MuseumContract.CATEGORY);

        return super.swapCursor(cursor);
    }

}
