/*
 * Copyright 2012 Riccardo Ciovati
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package it.rciovati.mappamusei;

import it.rciovati.mappamusei.util.ActivityHelper;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.support.v4.app.ShareCompat;
import android.webkit.WebView;


//XXX Change this when PreferenceFragment will be backported
@SuppressWarnings("deprecation")
public class AboutActivity extends PreferenceActivity {

    private static final String LICENSES_PATH = "file:///android_asset/licenses.html";

    private static final String INTENT_TYPE_TEXT_PLAIN = "text/plain";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //ActivityHelper.setBackgroundPatternTile(this);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        addPreferencesFromResource(R.xml.about);

        findPreference("licenses").setOnPreferenceClickListener(new OnPreferenceClickListener() {

            @Override
            public boolean onPreferenceClick(Preference preference) {
                WebView wv = new WebView(AboutActivity.this);
                wv.loadUrl(LICENSES_PATH);
                new AlertDialog.Builder(AboutActivity.this).setView(wv)
                        .setTitle(R.string.opensource_licenses)
                        .setPositiveButton(R.string.close, new OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
                return true;
            }
        });

        findPreference("iodl-license").setOnPreferenceClickListener(
                new OnPreferenceClickListener() {

                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        openBrowser("http://www.dati.gov.it/iodl/2.0/");
                        return true;
                    }
                });

        findPreference("contact").setOnPreferenceClickListener(new OnPreferenceClickListener() {

            @Override
            public boolean onPreferenceClick(Preference arg0) {

                ShareCompat.IntentBuilder builder = ShareCompat.IntentBuilder
                        .from(AboutActivity.this).setType(INTENT_TYPE_TEXT_PLAIN)
                        .addEmailTo(getString(R.string.author_email))
                        .setSubject(getString(R.string.email_subject));

                startActivity(builder.getIntent());

                return true;
            }
        });

        PackageInfo packageInfo = null;
        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (packageInfo != null) {
            String version = packageInfo.versionName;
            findPreference("version").setSummary(version);
            findPreference("version").setOnPreferenceClickListener(new OnPreferenceClickListener() {

                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri
                            .parse("market://details?id=" + getPackageName()));
                    marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY
                            | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

                    if (ActivityHelper.isThereAnActivityThatHandleIntent(AboutActivity.this,
                            marketIntent))
                        startActivity(marketIntent);
                    return false;
                }
            });
        }

        final String url = "https://dati.lombardia.it/dati/Cultura/Mappa-Musei/iu2c-9p5j";
        Preference datasetPreference = findPreference("dataset");

        datasetPreference.setSummary(url);
        datasetPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {

            @Override
            public boolean onPreferenceClick(Preference preference) {
                openBrowser(url);
                return true;
            }
        });

        findPreference("about").setOnPreferenceClickListener(new OnPreferenceClickListener() {

            @Override
            public boolean onPreferenceClick(Preference preference) {
                openBrowser("http://about.me/rciovati");

                return true;
            }
        });

        findPreference("share").setOnPreferenceClickListener(new OnPreferenceClickListener() {

            @Override
            public boolean onPreferenceClick(Preference arg0) {

                ShareCompat.IntentBuilder builder = ShareCompat.IntentBuilder
                        .from(AboutActivity.this).setType(INTENT_TYPE_TEXT_PLAIN)
                        .setText(getString(R.string.share_application_message))
                        .setChooserTitle(R.string.share_with);

                startActivity(builder.createChooserIntent());

                return true;
            }
        });

        findPreference("credits").setOnPreferenceClickListener(new OnPreferenceClickListener() {

            @Override
            public boolean onPreferenceClick(Preference preference) {
                openBrowser("http://androiduiux.com/");

                return true;
            }
        });

    }

    private void openBrowser(String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }
}
