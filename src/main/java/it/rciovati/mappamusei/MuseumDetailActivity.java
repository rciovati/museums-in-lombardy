/*
 * Copyright 2012 Riccardo Ciovati
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package it.rciovati.mappamusei;

import it.rciovati.mappamusei.fragments.MuseumDetailFragment;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;

public class MuseumDetailActivity extends BaseActivity {

    public static final String KEY_MUSEUM = "museum";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_museum_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Uri museumToDisplay = (Uri) getIntent().getExtras().getParcelable(KEY_MUSEUM);

        if (museumToDisplay == null) {
            throw new RuntimeException("A museum uri must be provided");
        }

        MuseumDetailFragment fragment = (MuseumDetailFragment) getSupportFragmentManager()
                .findFragmentById(R.id.detail_fragment);

        fragment.showMuseum(museumToDisplay);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
