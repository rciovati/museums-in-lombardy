/*
 * Copyright 2012 Riccardo Ciovati
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package it.rciovati.mappamusei.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ListAdapter;

import it.rciovati.mappamusei.R;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class StickyListHeadersListFragment extends Fragment {

    private View mProgressContainer;

    private StickyListHeadersListView mListView;

    private boolean mListShown;

    private View mListContainer;

    public StickyListHeadersListFragment() {
        super();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initList();
    }

    private void initList() {
        mProgressContainer = getView().findViewById(R.id.progressContainer);
        mListContainer = getView().findViewById(R.id.listContainer);

        mListView = (StickyListHeadersListView) getView().findViewById(android.R.id.list);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                onListItemClick(mListView, view, i, l);
            }
        });

        if (mProgressContainer == null || mListContainer == null) {
            throw new RuntimeException("Missing progress container or list container");
        }
    }

    public void setListAdapter(ListAdapter adapter) {
        if (adapter instanceof StickyListHeadersAdapter) {
            mListView.setAdapter((StickyListHeadersAdapter) adapter);
        }
    }

    public ListAdapter getListAdapter() {
        return mListView.getAdapter();
    }

    public void onListItemClick(StickyListHeadersListView listview, View view, int position, long id) {

    }

    public StickyListHeadersListView getListView() {
        return mListView;
    }

    public void setListShown(boolean shown) {
        setListShown(shown, true, true);
    }

    public void setListShown(boolean shown, boolean showProgressContainer) {
        setListShown(shown, true, showProgressContainer);
    }

    /**
     * Like {@link #setListShown(boolean)}, but no animation is used when
     * transitioning from the previous state.
     */
    public void setListShownNoAnimation(boolean shown, boolean showProgressContainer) {
        setListShown(shown, false, showProgressContainer);
    }

    /**
     * Control whether the list is being displayed. You can make it not
     * displayed if you are waiting for the initial data to show in it. During
     * this time an indeterminant progress indicator will be shown instead.
     *
     * @param shown   If true, the list view is shown; if false, the progress
     *                indicator. The initial value is true.
     * @param animate If true, an animation will be used to transition to the
     *                new state.
     */
    private void setListShown(boolean shown, boolean animate, boolean shownProgressContainer) {
        if (mProgressContainer == null) {
            throw new IllegalStateException("Can't be used with a custom content view");
        }
        if (mListShown == shown) {
            return;
        }
        mListShown = shown;
        if (shown) {
            if (animate) {
                mProgressContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(),
                        android.R.anim.fade_out));
                mListContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(),
                        android.R.anim.fade_in));
            } else {
                mProgressContainer.clearAnimation();
                mListContainer.clearAnimation();
            }
            mProgressContainer.setVisibility(View.GONE);
            mListContainer.setVisibility(View.VISIBLE);
        } else {
            if (animate) {
                mProgressContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(),
                        android.R.anim.fade_in));
                mListContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(),
                        android.R.anim.fade_out));
            } else {
                mProgressContainer.clearAnimation();
                mListContainer.clearAnimation();
            }

            if (shownProgressContainer)
                mProgressContainer.setVisibility(View.VISIBLE);
            mListContainer.setVisibility(View.GONE);
        }
    }

}
