/*
 * Copyright 2012 Riccardo Ciovati
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package it.rciovati.mappamusei.fragments;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.TouchDelegate;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import it.rciovati.mappamusei.BuildConfig;
import it.rciovati.mappamusei.MuseumActivity;
import it.rciovati.mappamusei.R;
import it.rciovati.mappamusei.provider.MuseumContract;
import it.rciovati.mappamusei.util.ActivityHelper;
import it.rciovati.mappamusei.util.Formatter;
import it.rciovati.mappamusei.util.ListTagHandler;

public class MuseumDetailFragment extends Fragment implements LoaderCallbacks<Cursor> {

    private static final String KEY_MUSEUM_URI = "museum";

    private static final int LOAD_MUSEUM = 1;

    Uri mMuseumUri;

    Cursor mMuseumCursor;

    TextView mNameTextView;

    String mLatitude;

    String mMLongitude;

    String mWebsite;

    String mPhone;

    private TextView mAccessibilityTextView;

    private TextView mLocationNameTextView;

    private TextView mOpeningHourTextView;

    private String mName;

    private TextView mWhereTextView;

    private TextView mInfoTextView;

    private LinearLayout mDetailContent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle b) {
        View v = inflater.inflate(R.layout.fragment_museum_detail, null);
        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        initComponents();
        super.onStart();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_museum_detail, menu);

        PackageManager pm = getActivity().getPackageManager();

        if (!pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY) || TextUtils.isEmpty(mPhone)) {
            menu.removeItem(R.id.menu_call);
        }

        if (TextUtils.isEmpty(mWebsite)) {
            menu.removeItem(R.id.menu_website);
        }

        MenuItem shareItem = menu.findItem(R.id.menu_share);
        AppCompatActivity activity = (AppCompatActivity) getContext();
        ShareActionProvider shareActionProvider = new ShareActionProvider(activity.getSupportActionBar().getThemedContext());
        shareActionProvider.setShareIntent(createShareIntent());
        MenuItemCompat.setActionProvider(shareItem, shareActionProvider);
    }

    private Intent createShareIntent() {

        ShareCompat.IntentBuilder builder = ShareCompat.IntentBuilder.from(getActivity()).setType(
                "text/plain");

        if (!TextUtils.isEmpty(mWebsite))
            builder.setText(getString(R.string.share_museum_with_url, mName, mWebsite));
        else {
            builder.setText(getString(R.string.share_museum, mName));
        }

        return builder.getIntent();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_directions:
                openNavigator();
                break;

            case R.id.menu_website:
                openBrowser();
                break;

            case R.id.menu_call:
                callMuseum();
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initComponents() {
        mAccessibilityTextView = (TextView) getView().findViewById(R.id.accessibility);
        mOpeningHourTextView = (TextView) getView().findViewById(R.id.opening_hour);
        mWhereTextView = (TextView) getView().findViewById(R.id.where);
        mInfoTextView = (TextView) getView().findViewById(R.id.info);
        mDetailContent = (LinearLayout) getView().findViewById(R.id.detail_content);
        //getSherlockActivity().setSupportProgressBarIndeterminateVisibility(true);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
        CursorLoader cl = new CursorLoader(getActivity());
        cl.setUri(mMuseumUri);
        return cl;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> arg0, Cursor cursor) {
        mMuseumCursor = cursor;
        updateUI();
        getActivity().supportInvalidateOptionsMenu();
    }

    private void updateUI() {
        if (mMuseumCursor != null && mMuseumCursor.getCount() == 1) {
            mMuseumCursor.moveToFirst();

            int nameIndex = mMuseumCursor.getColumnIndex(MuseumContract.NAME);
            int locationNameIndex = mMuseumCursor.getColumnIndex(MuseumContract.LOCATION_NAME);
            int openingHoursIndex = mMuseumCursor
                    .getColumnIndex(MuseumContract.OPENING_HOUR_DESCRIPTION);
            int openingHoursNotesIndex = mMuseumCursor
                    .getColumnIndex(MuseumContract.OPENING_HOUR_NOTES);
            int latIndex = mMuseumCursor.getColumnIndex(MuseumContract.LATITUDE);
            int lonIndex = mMuseumCursor.getColumnIndex(MuseumContract.LONGITUDE);
            int websiteIndex = mMuseumCursor.getColumnIndex(MuseumContract.WEBSITE);
            int phoneIndex = mMuseumCursor.getColumnIndex(MuseumContract.PHONE);

            int addressIndex = mMuseumCursor.getColumnIndex(MuseumContract.ADDRESS);
            int zipIndex = mMuseumCursor.getColumnIndex(MuseumContract.ZIP);
            int cityIndex = mMuseumCursor.getColumnIndex(MuseumContract.CITY);
            int provinceIndex = mMuseumCursor.getColumnIndex(MuseumContract.PROVINCE);

            mName = mMuseumCursor.getString(nameIndex);
            String museumLocationName = mMuseumCursor.getString(locationNameIndex);
            String openingHour = mMuseumCursor.getString(openingHoursIndex);
            String openingHourNotes = mMuseumCursor.getString(openingHoursNotesIndex);

            String address = mMuseumCursor.getString(addressIndex);
            String zip = mMuseumCursor.getString(zipIndex);
            String city = mMuseumCursor.getString(cityIndex);
            String province = mMuseumCursor.getString(provinceIndex);

            mWebsite = mMuseumCursor.getString(websiteIndex);
            sanitizeUrl();

            mPhone = mMuseumCursor.getString(phoneIndex);
            mLatitude = mMuseumCursor.getString(latIndex);
            mMLongitude = mMuseumCursor.getString(lonIndex);

            boolean isMultiPane = getResources().getBoolean(R.bool.has_two_panes);

            ViewStub vs = (ViewStub) getView().findViewById(R.id.header_view_stub);

            if (isMultiPane) {
                // if null means header_detail_view is in layout
                if (vs != null) {
                    vs.setLayoutResource(R.layout.header_detail_view);
                    vs.inflate();
                }

                final ImageButton button = (ImageButton) getView().findViewById(
                        R.id.close_details_button);
                button.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (getActivity() instanceof MuseumActivity) {
                            MuseumActivity a = (MuseumActivity) getActivity();
                            a.hideDetailFragment();
                        }
                    }
                });

                boolean isFloating = getResources().getBoolean(R.bool.floaing_detail_fragment);
                if (isFloating) {
                    button.setVisibility(View.VISIBLE);
                } else {
                    button.setVisibility(View.GONE);
                }

                increaseTouchArea(button);
            } else {
                vs.setLayoutResource(R.layout.header_museum);
                vs.inflate();
                mNameTextView = (TextView) getView().findViewById(R.id.museum_name);
                mLocationNameTextView = (TextView) getView()
                        .findViewById(R.id.museum_location_name);
                mNameTextView.setText(mName);

                if (museumLocationName != null
                        && museumLocationName.compareToIgnoreCase(mName) != 0) {
                    mLocationNameTextView.setText(museumLocationName);
                    mLocationNameTextView.setVisibility(View.VISIBLE);
                } else {
                    mLocationNameTextView.setVisibility(View.GONE);
                }
            }

            if (TextUtils.isEmpty(openingHour)) {
                openingHour = getString(R.string.no_opening_hour_available);
            }

            mOpeningHourTextView.setText(openingHour);

            if (!TextUtils.isEmpty(openingHourNotes)) {
                mOpeningHourTextView.append("\n\n");
                mOpeningHourTextView.append(openingHourNotes);
            }

            mAccessibilityTextView.setText(Html.fromHtml(buildAccessibilityDescription(), null,
                    new ListTagHandler()));

            String addressFormatted = Formatter
                    .formatAddressForDetail(address, zip, city, province);

            mWhereTextView.setText(Html.fromHtml(addressFormatted));

            String infoDescription = buildInfoDescription();
            mInfoTextView.setText(Html.fromHtml(infoDescription, null, new ListTagHandler()));

            mDetailContent.startAnimation(AnimationUtils.loadAnimation(getActivity(),
                    android.R.anim.fade_in));

            mDetailContent.setVisibility(View.VISIBLE);

            //getSherlockActivity().setSupportProgressBarIndeterminateVisibility(false);
        }
    }

    private void increaseTouchArea(final ImageButton button) {
        final View parent = (View) button.getParent();
        parent.post(new Runnable() {
            // Post in the parent's message queue to make sure the
            // parent
            // lays out its children before we call getHitRect()
            public void run() {
                final Rect r = new Rect();
                button.getHitRect(r);
                r.top -= 12;
                r.left -= 12;
                r.right += 12;
                r.bottom += 12;
                parent.setTouchDelegate(new TouchDelegate(r, button));
            }
        });
    }

    @Override
    public void onLoaderReset(Loader<Cursor> arg0) {
        // TODO Auto-generated method stub

    }

    public void showMuseum(Uri museumToDisplay) {

        if (BuildConfig.DEBUG) {
            Log.d("showMuseum", museumToDisplay.toString());
        }

        mMuseumUri = museumToDisplay;
        getLoaderManager().destroyLoader(LOAD_MUSEUM);
        getLoaderManager().initLoader(LOAD_MUSEUM, null, this);
    }

    @Override
    public void onSaveInstanceState(Bundle out) {
        if (mMuseumUri != null)
            out.putParcelable(KEY_MUSEUM_URI, mMuseumUri);
        super.onSaveInstanceState(out);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_MUSEUM_URI)) {
            Uri uri = (Uri) savedInstanceState.getParcelable(KEY_MUSEUM_URI);
            if (uri != null) {
                showMuseum(uri);
            }
        }

        // we need this because android:textAllCaps="true" works only for
        // Android >= ics
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            setAllCaps(R.id.opening_hours_header);
            setAllCaps(R.id.accessibility_header);
            setAllCaps(R.id.where_header);
            setAllCaps(R.id.info_header);
        }

        super.onActivityCreated(savedInstanceState);
    }

    private void setAllCaps(int resId) {
        TextView tv = (TextView) getView().findViewById(resId);
        tv.setText(tv.getText().toString().toUpperCase());
    }

    public void openNavigator() {
        String query = "google.navigation:q=" + mLatitude + "," + mMLongitude;
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(query));
        if (ActivityHelper.isThereAnActivityThatHandleIntent(getActivity(), i))
            startActivity(i);
    }

    public void callMuseum() {
        Intent i = new Intent(Intent.ACTION_DIAL);
        String p = "tel:" + mPhone;
        i.setData(Uri.parse(p));
        startActivity(i);
    }

    public void openBrowser() {
        sanitizeUrl();
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mWebsite));
        startActivity(browserIntent);
    }

    private void sanitizeUrl() {
        if (TextUtils.isEmpty(mWebsite))
            return;

        if (!mWebsite.startsWith("http://") && !mWebsite.startsWith("https://"))
            mWebsite = "http://" + mWebsite;
    }

    private String buildAccessibilityDescription() {
        int parkingsIndex = mMuseumCursor.getColumnIndex(MuseumContract.PARKING_FOR_DISABLED);
        int accessibility = mMuseumCursor.getColumnIndex(MuseumContract.ACCESSIBLE_TO_DISABLED);

        String[] accessibilityLevels = getResources().getStringArray(R.array.accessibility_levels);
        String[] parkingLevels = getResources().getStringArray(R.array.parking_levels);

        int parkingValue = mMuseumCursor.getInt(parkingsIndex);
        int accessibilityValue = mMuseumCursor.getInt(accessibility);

        return String.format("<ul><li>%s</li><li>%s</li></ul>",
                accessibilityLevels[accessibilityValue], parkingLevels[parkingValue]);

    }

    private String buildInfoDescription() {
        int isFreeIndex = mMuseumCursor.getColumnIndex(MuseumContract.FREE_ENTRY);
        int guidedVisitsIndex = mMuseumCursor.getColumnIndex(MuseumContract.GUIDED_VISIT);
        int parkingsIndex = mMuseumCursor.getColumnIndex(MuseumContract.PARKINGS);
        int libraryIndex = mMuseumCursor.getColumnIndex(MuseumContract.LIBRARY);
        int educationalIndex = mMuseumCursor.getColumnIndex(MuseumContract.EDUCATIONAL_ACTIVITIES);
        int studyRoomIndex = mMuseumCursor.getColumnIndex(MuseumContract.STUDY_ROOM);

        int isFree = mMuseumCursor.getInt(isFreeIndex);
        int parkings = mMuseumCursor.getInt(parkingsIndex);
        int guidedVisits = mMuseumCursor.getInt(guidedVisitsIndex);
        int library = mMuseumCursor.getInt(libraryIndex);
        int educationalActivities = mMuseumCursor.getInt(educationalIndex);
        int studyRoom = mMuseumCursor.getInt(studyRoomIndex);

        StringBuilder sb = new StringBuilder();

        if (isFree == 1) {
            sb.append(getString(R.string.the_museum_is_free));
        }

        if (guidedVisits == 1) {
            sb.append(getString(R.string.guided_visits_available));
        }

        if (library == 1) {
            sb.append(getString(R.string.library));
        }

        if (educationalActivities == 1) {
            sb.append(getString(R.string.educational_activities));
        }

        if (studyRoom == 1) {
            sb.append(getString(R.string.study_room));
        }

        if (parkings == 1) {
            sb.append(getString(R.string.parking_available));
        }

        if (sb.length() > 0) {
            sb.insert(0, "<ul>");
            sb.append("</ul>");
        }

        return sb.toString();

    }
}
