/*
 * Copyright 2012 Riccardo Ciovati
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package it.rciovati.mappamusei.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;

import it.rciovati.mappamusei.R;
import it.rciovati.mappamusei.events.OnSortTypeChanged;
import it.rciovati.mappamusei.util.SortType;


public class SortOptionsDialogFragment extends AppCompatDialogFragment {

    private OnSortTypeChanged mCallBack;

    public SortOptionsDialogFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (activity instanceof OnSortTypeChanged) {
            mCallBack = (OnSortTypeChanged) activity;
        }

    }

    public static SortOptionsDialogFragment newInstance() {
        SortOptionsDialogFragment d = new SortOptionsDialogFragment();
        return d;
    }

    @Override
    public Dialog onCreateDialog(Bundle b) {
        final CharSequence[] items = {
                getString(R.string.sort_by_province), getString(R.string.sort_by_category)
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.sort_title);
        SortType actualSortType = (SortType) getArguments().getSerializable("s");
        int defaultSelected = actualSortType == SortType.BY_PROVINCE ? 0 : 1;
        builder.setSingleChoiceItems(items, defaultSelected, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                SortType selected = item == 0 ? SortType.BY_PROVINCE : SortType.BY_CATEGORY;
                if (mCallBack != null) {
                    mCallBack.onSortTypeChanged(selected);
                }
                dialog.dismiss();
            }
        });

        Dialog d = builder.create();
        d.setCanceledOnTouchOutside(true);

        return d;
    }

}
