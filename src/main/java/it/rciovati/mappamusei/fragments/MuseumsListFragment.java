/*
 * Copyright 2012 Riccardo Ciovati
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package it.rciovati.mappamusei.fragments;

import it.rciovati.mappamusei.MappaMuseiApplication;
import it.rciovati.mappamusei.MuseumActivity;
import it.rciovati.mappamusei.R;
import it.rciovati.mappamusei.adapter.MuseumsAdapter;
import it.rciovati.mappamusei.adapter.MuseumsAdapter.AdapterMode;
import it.rciovati.mappamusei.events.OnMuseumSelectedListener;
import it.rciovati.mappamusei.provider.MuseumContract;
import it.rciovati.mappamusei.util.Filter;
import it.rciovati.mappamusei.util.SortType;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class MuseumsListFragment extends StickyListHeadersListFragment implements LoaderCallbacks<Cursor> {

    private static final String KEY_FILTER = "filter";

    private static final String KEY_FIRST_VISIBLE_POSITION = "pos";

    private static final String KEY_SORT = "sort";

    private static final int MUSEUMS_LIST_LOADER = 2;

    protected boolean mDisableSortOrderChange;

    private int mFirstVisiblePosition = -1;

    private MuseumsAdapter mMuseumsAdapter;

    private OnMuseumSelectedListener mMuseumSelectedCallback;

    protected String mSearchQuery = "";

    private boolean mSearchStarted = false;

    private SortType mSortType;

    private Filter mFilter = Filter.EMPTY;

    private static final String[] PROJECTION = new String[] {
            MuseumContract._ID, MuseumContract.NAME, MuseumContract.CATEGORY,
            MuseumContract.LOCATION_NAME, MuseumContract.FREE_ENTRY, MuseumContract.PROVINCE,
            MuseumContract.CITY
    };

    protected void filterListView(String newText) {
        setListShownNoAnimation(false, false);
        mSearchStarted = true;
        doLoad();
    }

    private void doLoad() {
        getLoaderManager().destroyLoader(MUSEUMS_LIST_LOADER);
        getLoaderManager().initLoader(MUSEUMS_LIST_LOADER, Bundle.EMPTY, this);
    }

    private void loadMuseums() {
        setListShownNoAnimation(false, true);
        doLoad();
    }

    private void loadMuseumsAfterSearch() {
        setListShownNoAnimation(false, false);
        doLoad();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mSortType = SortType.BY_PROVINCE;

        if (savedInstanceState != null) {

            if (savedInstanceState.containsKey(KEY_SORT)) {
                mSortType = (SortType) savedInstanceState.getSerializable(KEY_SORT);
            }

            if (savedInstanceState.containsKey(KEY_FIRST_VISIBLE_POSITION)) {
                mFirstVisiblePosition = savedInstanceState.getInt(KEY_FIRST_VISIBLE_POSITION);
            }

            if (savedInstanceState.containsKey(KEY_FILTER)) {
                mFilter = (Filter) savedInstanceState.getSerializable(KEY_FILTER);
            }
        }

        mMuseumsAdapter = new MuseumsAdapter(getActivity());
        setListAdapter(mMuseumsAdapter);

        loadMuseums();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mMuseumSelectedCallback = (OnMuseumSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnMuseumSelectedListener");
        }
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setHasOptionsMenu(true);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        String sortOrder;
        StringBuilder selection = new StringBuilder();

        CursorLoader cl = new CursorLoader(getContext());
        cl.setUri(MuseumContract.CONTENT_URI);
        cl.setProjection(PROJECTION);

        if (mSearchStarted) {
            selection.append(MuseumContract.NAME);
            selection.append(" LIKE \"%");
            selection.append(mSearchQuery);
            selection.append("%\"");
        }

        if (mSortType == SortType.BY_PROVINCE) {
            sortOrder = String.format("%s ASC, %s ASC", MuseumContract.PROVINCE,
                    MuseumContract.CITY);
        } else {
            sortOrder = String.format("%s ASC, %s ASC, %s ASC", MuseumContract.CATEGORY,
                    MuseumContract.PROVINCE, MuseumContract.CITY);
        }

        if (mFilter != Filter.EMPTY) {
            selection = mFilter.addToSelection(selection);
        }

        if (selection.length() > 0) {
            cl.setSelection(selection.toString());
        }

        cl.setSortOrder(sortOrder);

        return cl;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_museums_list, menu);
        SearchView searchView = new SearchView(getContext());
        menu.findItem(R.id.menu_search).setActionView(searchView);
        searchView.setIconifiedByDefault(true);
        searchView.setQueryHint(getContext().getString(R.string.find_museum));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextChange(String newText) {

                if (!newText.trim().equals(mSearchQuery)) {

                    mSearchQuery = newText.trim();

                    if (newText.length() > 0) {
                        mDisableSortOrderChange = true;
                        filterListView(newText);
                    } else {
                        mSearchStarted = false;
                        loadMuseumsAfterSearch();
                        mDisableSortOrderChange = false;
                    }
                }

                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {

            @Override
            public boolean onClose() {
                if (mSearchStarted != false) {
                    mSearchStarted = false;
                    loadMuseumsAfterSearch();
                }
                return false;
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle args) {
        // Bundle b = getArguments();
        // if(b != null && b.containsKey(KEY_FILTER)){
        // mFilter = (Filter) b.getSerializable(KEY_FILTER);
        // }

        MappaMuseiApplication app = (MappaMuseiApplication) getActivity().getApplication();
        mFilter = app.getMuseumsFilter();

        return inflater.inflate(R.layout.fragment_museums_list, parent, false);
    }

    @Override
    public void onDetach() {
        getLoaderManager().destroyLoader(MUSEUMS_LIST_LOADER);
        super.onDetach();
    }

    @Override
    public void onListItemClick(StickyListHeadersListView listview, View view, int position, long id) {
        Uri museumUri = ContentUris.withAppendedId(MuseumContract.CONTENT_URI, id);
        mMuseumSelectedCallback.onMuseumSelected(this, view, museumUri);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // TBD
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

        if (cursor == null || cursor.getCount() == 0) {
            getListView().setEmptyView(getView().findViewById(R.id.empty));
        } else {
            getView().findViewById(R.id.empty).setVisibility(View.GONE);
            getListView().setEmptyView(null);
        }

        AdapterMode mode = mSortType == SortType.BY_PROVINCE ? AdapterMode.SORTED_BY_PROVINCE
                : AdapterMode.SORTED_BY_CATEGORY;

        mMuseumsAdapter.setCursor(cursor, mode);

        if (mSearchStarted) {
            mMuseumsAdapter.setSearchQuery(mSearchQuery);
        } else {
            mMuseumsAdapter.resetSearchQuery();
        }

        getListView().setSelectionFromTop(0, 0);

        if (mFirstVisiblePosition != -1) {
            getListView().setSelectionFromTop(mFirstVisiblePosition, 0);
            mFirstVisiblePosition = -1;
        }

        if (isResumed() && !mSearchStarted) {
            setListShown(true);
        } else {
            setListShownNoAnimation(true, true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_sort) {

            if (mDisableSortOrderChange) {
                Toast.makeText(getContext(), R.string.cant_change_sort_during_search,
                        Toast.LENGTH_SHORT).show();
                return true;
            }

            Context activity = getContext();

            if (activity instanceof MuseumActivity) {
                ((MuseumActivity) activity).displayMuseumsSortTypeDialog(mSortType);
            }

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle out) {
        out.putSerializable(KEY_SORT, mSortType);

        if (mFilter != Filter.EMPTY)
            out.putSerializable(KEY_FILTER, mFilter);

        if (!this.isDetached())
            out.putInt(KEY_FIRST_VISIBLE_POSITION, getListView().getFirstVisiblePosition());
        super.onSaveInstanceState(out);
    }

    public void setSortType(SortType type) {
        if (mSortType != type) {
            mSortType = type;
            loadMuseums();
        }
    }

    public void selectMuseum(Uri museumUri) {
        Long id = ContentUris.parseId(museumUri);

        long[] checkedItemsIds = getListView().getCheckedItemIds();

        if (checkedItemsIds.length == 0 || id != checkedItemsIds[0])
            new SelectMuseumAsyncTask().execute(id);
    }

    private class SelectMuseumAsyncTask extends AsyncTask<Long, Void, Integer> {

        @Override
        protected void onPostExecute(Integer result) {
            if (result != -1) {
                getListView().setItemChecked(result, true);
                getListView().setSelectionFromTop(result, -1);
            }
            super.onPostExecute(result);
        }

        @Override
        protected Integer doInBackground(Long... params) {
            // TODO find a better way to search the position from the item id

            CursorAdapter adapter = (CursorAdapter) getListAdapter();

            Cursor cursor = adapter.getCursor();
            long id = params[0];

            int idIndex = cursor.getColumnIndex(MuseumContract._ID);
            cursor.moveToFirst();
            int i = 0;

            do {
                if (cursor.getLong(idIndex) == id)
                    return i;
                i++;
            } while (cursor.moveToNext());

            return -1;
        }

    }

    public void filter(Filter filter) {
        mFilter = filter;
        loadMuseums();
    }

}
