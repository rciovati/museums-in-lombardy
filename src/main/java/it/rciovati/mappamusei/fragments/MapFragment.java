/*
 * Copyright 2012 Riccardo Ciovati
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package it.rciovati.mappamusei.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import it.rciovati.mappamusei.MappaMuseiApplication;
import it.rciovati.mappamusei.R;
import it.rciovati.mappamusei.events.OnMuseumSelectedListener;
import it.rciovati.mappamusei.provider.MuseumContract;
import it.rciovati.mappamusei.util.Filter;

import java.util.HashMap;
import java.util.Map;

public class MapFragment extends SupportMapFragment implements LoaderCallbacks<Cursor>, GoogleMap.OnMarkerClickListener {

    private static final String KEY_FILTER = "filter";

    protected static final String SEPARATOR = " - ";

    private static final int LOAD_MUSEUMS = 1;

    static final String LOG_TAG = MapFragment.class.getCanonicalName();

    private long mDefaultFocusedId = -1;

    OnMuseumSelectedListener mOnMuseumSelectedCallBack;

    private Filter mFilter = Filter.EMPTY;

    private boolean mIsRefreshing = false;

    private HashMap<Marker, Long> mIds;

    private GoogleMap mMap;
    private Long selectedMarkerId;

    public long getFocusedMuseumId() {
        return selectedMarkerId != null ? selectedMarkerId : -1;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        if (savedInstanceState != null) {

            if (savedInstanceState.containsKey("id")) {

                mDefaultFocusedId = savedInstanceState.getLong("id");
            }

            if (savedInstanceState.containsKey(KEY_FILTER)) {
                mFilter = (Filter) savedInstanceState.getSerializable(KEY_FILTER);
            }
        }

        setUpMapIfNeeded();

        loadMuseums();

        super.onActivityCreated(savedInstanceState);
    }


    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mMap = googleMap;
                    setUpMap();
                }
            });
        }
    }

    private void setUpMap() {

        mMap.setOnMarkerClickListener(this);
    }

    private void loadMuseums() {
        mIsRefreshing = true;
        getLoaderManager().destroyLoader(LOAD_MUSEUMS);
        getLoaderManager().initLoader(LOAD_MUSEUMS, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        String[] PROJECTION = new String[]{
                MuseumContract._ID, MuseumContract.NAME, MuseumContract.LATITUDE,
                MuseumContract.LONGITUDE, MuseumContract.ADDRESS, MuseumContract.CATEGORY,
                MuseumContract.CITY, MuseumContract.LOCATION_NAME, MuseumContract.PROVINCE
        };

        StringBuilder selection = mFilter.addToSelection(null);

        return new CursorLoader(getActivity(), MuseumContract.CONTENT_URI, PROJECTION,
                selection.toString(), null, MuseumContract._ID + " ASC");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup vg, Bundle data) {

        MappaMuseiApplication app = (MappaMuseiApplication) getActivity().getApplication();
        mFilter = app.getMuseumsFilter();

        return super.onCreateView(inflater, vg, data);
    }

    @Override
    public void onDetach() {
        getLoaderManager().destroyLoader(LOAD_MUSEUMS);
        super.onDetach();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        populateMuseumsItemizedOverlay(cursor);
    }

    @Override
    public void onSaveInstanceState(Bundle out) {
        long focusedIndex = getFocusedMuseumId();

        if (focusedIndex != -1) {
            out.putLong("id", focusedIndex);
        }

        if (mFilter != Filter.EMPTY)
            out.putSerializable(KEY_FILTER, mFilter);

        super.onSaveInstanceState(out);
    }

    private void populateMuseumsItemizedOverlay(Cursor cursor) {

        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();

        if (cursor != null && cursor.moveToFirst()) {

            mIds = new HashMap<Marker, Long>(cursor.getCount());

            int count = 0;

            do {

                count++;
                long id = cursor.getLong(0);
                double latitude = cursor.getDouble(2);
                double longitude = cursor.getDouble(3);
                String name = cursor.getString(1);

                LatLng position = new LatLng(latitude, longitude);
                MarkerOptions currentItemMarkerOptions = new MarkerOptions().position(position).title(name);

                Marker m = mMap.addMarker(currentItemMarkerOptions);
                mIds.put(m, id);

                boundsBuilder.include(position);

            } while (cursor.moveToNext());

            if (count > 0) {
                setZoomToSpan(boundsBuilder.build());
            }
        }

        if (mDefaultFocusedId != -1) {
            setFocusOnMuseum(mDefaultFocusedId);
            mDefaultFocusedId = -1;
        }

        mIsRefreshing = false;

    }

    private void setZoomToSpan(final LatLngBounds bounds) {
        try {
            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));
        } catch (IllegalStateException e) {
            final View mapView = getView();
            if (mapView.getViewTreeObserver().isAlive()) {
                mapView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                    @SuppressWarnings("deprecation")
                    @SuppressLint("NewApi")
                    @Override
                    public void onGlobalLayout() {
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            mapView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        } else {
                            mapView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
                        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));
                    }
                });
            }
        }
    }

    public void setFocusOnMuseum(final long museumId) {

        long focusedId = getFocusedMuseumId();

        if (focusedId != museumId) {

            Marker m = getKeyByValue(mIds, museumId);
            m.showInfoWindow();

            setCenterWithOffset(m.getPosition());

            selectedMarkerId = museumId;
        }
    }

    public void setCenterWithOffset(LatLng point) {

        boolean floatingDetailFragment = getResources().getBoolean(R.bool.floaing_detail_fragment);

        if (floatingDetailFragment) {

            int detailFragmentHeight = getResources().getDimensionPixelOffset(R.dimen.detail_fragment_height);

            int offset = (int) (detailFragmentHeight + dp2px(30));
            mMap.setPadding(0, 0, offset, 0);
        } else {
            mMap.setPadding(0, 0, 0, 0);
        }

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(point)      // Sets the center of the map to Mountain View
                .zoom(10)                   // Sets the zoom
                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    public void setFocusOnMuseum(Uri museumUri) {
        long id = ContentUris.parseId(museumUri);
        setFocusOnMuseum(id);
    }

    public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
        for (Map.Entry<T, E> entry : map.entrySet()) {
            if (value.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (activity instanceof OnMuseumSelectedListener) {
            mOnMuseumSelectedCallBack = (OnMuseumSelectedListener) activity;
        }
    }

    public void filter(Filter filter) {
        mFilter = filter;
        mDefaultFocusedId = -1;
        loadMuseums();
    }

    public boolean isRefreshing() {
        return mIsRefreshing;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        selectedMarkerId = mIds.get(marker);

        setCenterWithOffset(marker.getPosition());

        mOnMuseumSelectedCallBack.onMuseumSelected(this, null, ContentUris.withAppendedId(MuseumContract.CONTENT_URI, selectedMarkerId));

        return false;
    }

    private float dp2px(float dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }
}
