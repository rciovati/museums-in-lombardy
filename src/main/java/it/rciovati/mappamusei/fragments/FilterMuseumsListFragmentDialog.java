/*
 * Copyright 2012 Riccardo Ciovati
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package it.rciovati.mappamusei.fragments;

import it.rciovati.mappamusei.MuseumActivity;
import it.rciovati.mappamusei.R;
import it.rciovati.mappamusei.provider.MuseumContract;
import it.rciovati.mappamusei.util.Filter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Spinner;


public class FilterMuseumsListFragmentDialog extends AppCompatDialogFragment implements
        LoaderCallbacks<Cursor> {

    private static final String KEY_FILTER = "filter";

    private static final int CATEGORIES_LOADER = 1;

    private static final int CITY_LOADER = 2;

    private CursorLoader mCategoriesLoader;

    private CursorLoader mProvinceLoader;

    private ArrayAdapter<String> mProvincesAdapter;

    private ArrayAdapter<String> mCategoriesAdapter;

    protected String mProvinceSelected;

    protected String mCategorySelected;

    private Spinner mProvincesSpinner;

    private Spinner mCategoriesSpinner;

    private CheckBox mOnlyFreeCheckBox;

    MuseumActivity mCallBack;

    private Filter mCurrentFilter = Filter.EMPTY;

    protected boolean mOnlyFree = false;

    public FilterMuseumsListFragmentDialog() {

    }

    public static FilterMuseumsListFragmentDialog newInstance() {
        return new FilterMuseumsListFragmentDialog();
    }

    @Override
    public void onStart() {
        super.onStart();
        getLoaderManager().initLoader(CATEGORIES_LOADER, null, this);
        getLoaderManager().initLoader(CITY_LOADER, null, this);
    }

    @Override
    public void onAttach(Activity activity) {
        if (activity instanceof MuseumActivity) {
            mCallBack = (MuseumActivity) activity;
        }
        super.onAttach(activity);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        mCurrentFilter = (Filter) getArguments().getSerializable(KEY_FILTER);

        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_FILTER)) {
            //this would come from orientation change
            mCurrentFilter = (Filter) savedInstanceState.getSerializable(KEY_FILTER);
        }

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.filter_list_dialog, null);

        mProvincesSpinner = (Spinner) view.findViewById(R.id.province_spinner);
        mCategoriesSpinner = (Spinner) view.findViewById(R.id.category_spinner);
        mOnlyFreeCheckBox = (CheckBox) view.findViewById(R.id.only_free_museums);

        mProvincesAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item);
        mProvincesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mCategoriesAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item);
        mCategoriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mProvincesSpinner.setAdapter(mProvincesAdapter);
        mCategoriesSpinner.setAdapter(mCategoriesAdapter);

        mOnlyFreeCheckBox.setChecked(mCurrentFilter.isOnlyFree());
        mOnlyFreeCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mOnlyFree = isChecked;

            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.setTitle(R.string.filter_list);
        builder.setPositiveButton(R.string.confirm, new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                boolean onlyFree = mOnlyFreeCheckBox.isChecked();

                if (mCallBack != null) {
                    Filter f = new Filter(mProvinceSelected, mCategorySelected, onlyFree);
                    mCallBack.filter(f);
                }

            }
        });
        builder.setNegativeButton(R.string.reset, new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (mCallBack != null) {
                    mCallBack.filter(Filter.EMPTY);
                }
            }
        });

        Dialog d = builder.create();
        d.setCanceledOnTouchOutside(true);
        return d;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == CITY_LOADER) {
            mProvinceLoader = new CursorLoader(getActivity(), MuseumContract.Provinces.CONTENT_URI,
                    null, null, null, null);
            return mProvinceLoader;
        } else {
            mCategoriesLoader = new CursorLoader(getActivity(),
                    MuseumContract.Categories.CONTENT_URI, null, null, null, null);
            return mCategoriesLoader;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (loader == mProvinceLoader) {

            List<String> provinces = buildListFromCursor(MuseumContract.Provinces.NAME, cursor);
            provinces.add(0, getString(R.string.all));

            int index = 0;
            int selectedProv = 0;
            for (String prov : provinces) {

                if (mCurrentFilter.getProvince() != null
                        && mCurrentFilter.getProvince().equals(prov)) {
                    selectedProv = index;
                }

                mProvincesAdapter.add(prov);
                index++;
            }

            mProvincesSpinner.setSelection(selectedProv);
            mProvincesSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    if (arg2 > 0) {
                        mProvinceSelected = mProvincesAdapter.getItem(arg2);
                    } else {
                        mProvinceSelected = null;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                    mProvinceSelected = "";
                }
            });

        } else {
            List<String> categories = buildListFromCursor(MuseumContract.Categories.NAME, cursor);
            categories.add(0, getString(R.string.all));

            int index = 0;
            int selectedCat = 0;

            for (String cat : categories) {
                if (mCurrentFilter.getCategory() != null
                        && mCurrentFilter.getCategory().equals(cat)) {
                    selectedCat = index;
                }
                mCategoriesAdapter.add(cat);
                index++;
            }

            mCategoriesSpinner.setSelection(selectedCat);
            mCategoriesSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    if (arg2 > 0) {
                        mCategorySelected = mCategoriesAdapter.getItem(arg2);
                    } else {
                        mCategorySelected = null;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                    mCategorySelected = "";
                }
            });
        }
    }

    private List<String> buildListFromCursor(String indexName, Cursor cursor) {
        if (cursor != null) {
            List<String> strings = new ArrayList<String>(cursor.getCount());

            cursor.moveToFirst();
            int index = cursor.getColumnIndex(indexName);
            do {
                strings.add(cursor.getString(index));
            } while (cursor.moveToNext());

            return strings;
        } else {
            return new ArrayList<String>(0);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Filter savedFilter = new Filter(mProvinceSelected, mCategorySelected, mOnlyFree);
        outState.putSerializable(KEY_FILTER, savedFilter);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance())
            getDialog().setOnDismissListener(null);
        getLoaderManager().destroyLoader(CATEGORIES_LOADER);
        getLoaderManager().destroyLoader(CITY_LOADER);
        super.onDestroyView();
    }

}
