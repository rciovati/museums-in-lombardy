/*
 * Copyright 2012 Riccardo Ciovati
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package it.rciovati.mappamusei.util;

import org.xml.sax.XMLReader;

import android.text.Editable;
import android.text.Html.TagHandler;

public class ListTagHandler implements TagHandler {
    private static final String OL = "ol";

    private static final String LI = "li";

    private static final String UL = "ul";
   
    boolean first = true;

    String parent = null;

    int index = 1;

    int ulCount = 1;

    @Override
    public void handleTag(boolean opening, String tag, Editable output, XMLReader xmlReader) {

        if (tag.equals(UL))
            parent = UL;
        else if (tag.equals(OL))
            parent = OL;
        if (tag.equals(LI)) {
            if (parent.equals(UL)) {
                if (first) {
                    if (ulCount == 1)
                        output.append("\u2022 ");
                    else
                        output.append("\n\u2022 ");
                    ulCount++;
                    first = false;
                } else {
                    first = true;
                }
            } else {
                if (first) {
                    output.append("\n\t" + index + ". ");
                    first = false;
                    index++;
                } else {
                    first = true;
                }
            }
        }
    }
}
