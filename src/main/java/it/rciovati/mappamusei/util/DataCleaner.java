/*
 * Copyright 2012 Riccardo Ciovati
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package it.rciovati.mappamusei.util;

import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.i18n.phonenumbers.PhoneNumberMatch;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;

public class DataCleaner {

	public static final Pattern emailPattern = Pattern
			.compile("(\\w+)(\\.\\w+)*@(\\w+\\.)(\\w+)(\\.\\w+)*");

	public static final Pattern validEmailPattern = Pattern.compile(
			"^[\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$",
			Pattern.CASE_INSENSITIVE);

	public static final Pattern webSitePattern = Pattern
			.compile("\\(?\\b(http://|www[.])[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]");

	public static final String IT = "IT";

	public static String extractEmail(String content) {
		String email = null;
		Matcher matcher = emailPattern.matcher(content);
		while (matcher.find()) {

			email = matcher.group();

			if (isValidEmailAddress(email)) {
				return email;
			}

		}
		return content;
	}

	public static String extractWebSite(String content) {
		Matcher matcher = webSitePattern.matcher(content);
		while (matcher.find()) {
			return matcher.group();
		}
		return content;
	}

	public static String extractPhoneNumber(String content) {

		PhoneNumberUtil instance = PhoneNumberUtil.getInstance();

		Iterable<PhoneNumberMatch> matches = instance.findNumbers(content, IT);

		Iterator<PhoneNumberMatch> iterator = matches.iterator();

		while (iterator.hasNext()) {
			return instance.format(iterator.next().number(),
					PhoneNumberFormat.INTERNATIONAL);
		}

		return content;
	}

	private static boolean isValidEmailAddress(String emailAddress) {
		Matcher matcher = validEmailPattern.matcher(emailAddress);
		return matcher.matches();
	}

}
