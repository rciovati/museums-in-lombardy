/*
 * Copyright 2012 Riccardo Ciovati
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package it.rciovati.mappamusei.util;

import it.rciovati.mappamusei.provider.MuseumContract;
import android.net.Uri;

public class UriUtils {

    public static Uri buildMuseumsContentUriWithUserCoordinates(double lat, double lon) {
        return MuseumContract.CONTENT_URI.buildUpon()
                .appendQueryParameter(MuseumContract.USER_LAT, String.valueOf(lat))
                .appendQueryParameter(MuseumContract.USER_LON, String.valueOf(lon)).build();
    }

}
