/*
 * Copyright 2012 Riccardo Ciovati
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package it.rciovati.mappamusei.util;

import android.support.v4.util.LruCache;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;

public class Formatter {

    private static final String BR = "<br>";

    private static final String RIGHT_BRACKET = ")";

    private static final String LEFT_BRACKET = " (";

    private static final StringBuffer mStringBuffer = new StringBuffer();

    private static final LruCache<String, ForegroundColorSpan> mSpannabledCache = new LruCache<String, ForegroundColorSpan>(
            20);

    protected static final SpannableStringBuilder mSpannableStringBuilder = new SpannableStringBuilder();

    protected static final StyleSpan mBoldSpan = new StyleSpan(android.graphics.Typeface.BOLD);

    public static String formatCityAndProvince(String city, String province) {
        mStringBuffer.setLength(0);
        mStringBuffer.append(city);

        if (city.compareToIgnoreCase(province) != 0) {
            mStringBuffer.append(LEFT_BRACKET);
            mStringBuffer.append(province);
            mStringBuffer.append(RIGHT_BRACKET);
        }

        return mStringBuffer.toString();
    }

    public static SpannableStringBuilder formatListCategory(String category) {
        mSpannableStringBuilder.clear();

        ForegroundColorSpan sp;

        sp = mSpannabledCache.get(category);

        if (sp == null) {
            int color = ColorUtils.fromString(category);
            sp = new ForegroundColorSpan(color);
            mSpannabledCache.put(category, sp);
        }

        mSpannableStringBuilder.append(category.toUpperCase());

        int lenght = category.length();

        mSpannableStringBuilder.setSpan(sp, 0, lenght, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        mSpannableStringBuilder.setSpan(mBoldSpan, 0, mSpannableStringBuilder.length(),
                Spannable.SPAN_INCLUSIVE_EXCLUSIVE);

        return mSpannableStringBuilder;
    }

    public static String formatAddressForDetail(String address, String zip, String city,
            String province) {
        StringBuilder builder = new StringBuilder();
        builder.append(address);
        builder.append(BR);
        if (!TextUtils.isEmpty(zip)) {
            builder.append(zip);
            builder.append(BR);
        }
        builder.append(formatCityAndProvince(city, province));
        return builder.toString();

    }
}
