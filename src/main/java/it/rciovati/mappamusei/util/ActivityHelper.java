/*
 * Copyright 2012 Riccardo Ciovati
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package it.rciovati.mappamusei.util;

import java.util.List;

import it.rciovati.mappamusei.R;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;

public class ActivityHelper {

    /*
    public static void setBackgroundPatternTile(SherlockFragmentActivity a) {
        // This is a workaround for http://b.android.com/15340 from
        // http://stackoverflow.com/a/5852198/132047
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            BitmapDrawable bg = (BitmapDrawable) a.getResources().getDrawable(
                    R.drawable.actionbar_background_pattern_tile);
            bg.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
            a.getSupportActionBar().setBackgroundDrawable(bg);
        }
    }

    public static void setBackgroundPatternTile(SherlockPreferenceActivity a) {
        // This is a workaround for http://b.android.com/15340 from
        // http://stackoverflow.com/a/5852198/132047
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            BitmapDrawable bg = (BitmapDrawable) a.getResources().getDrawable(
                    R.drawable.actionbar_background_pattern_tile);
            bg.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
            a.getSupportActionBar().setBackgroundDrawable(bg);
        }
    }
*/
    public static boolean isThereAnActivityThatHandleIntent(Context c, Intent i) {
        List<ResolveInfo> queryIntentActivities = c.getPackageManager().queryIntentActivities(i, 0);
        if (queryIntentActivities != null && queryIntentActivities.size() > 0) {
            return true;
        }
        return false;
    }

}
