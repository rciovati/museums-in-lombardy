/*
 * Copyright 2012 Riccardo Ciovati
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package it.rciovati.mappamusei.util;

import java.io.Serializable;

import it.rciovati.mappamusei.provider.MuseumContract;
import android.content.SharedPreferences;
import android.text.TextUtils;

public class Filter implements Serializable {

    private static final long serialVersionUID = 5294633873933081792L;

    private static final String QUOTE_CLOSE = "\"";

    private static final String EQUAL_QUOTE = " = \"";

    private static final String EQUAL = " = ";

    private static final String AND = " AND ";

    public static final Filter EMPTY = new Filter(null, null, false);

    private String province;

    private String category;

    private boolean onlyFree;

    public Filter(String province, String category, boolean onlyFree) {
        this.province = province;
        this.category = category;
        this.onlyFree = onlyFree;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isOnlyFree() {
        return onlyFree;
    }

    public void setOnlyFree(boolean onlyFree) {
        this.onlyFree = onlyFree;
    }

    public StringBuilder addToSelection(StringBuilder selection) {

        if (selection == null) {
            selection = new StringBuilder(0);
        }

        if (onlyFree) {
            if (selection.length() > 0) {
                selection.append(AND);
            }

            selection.append(MuseumContract.FREE_ENTRY);
            selection.append(EQUAL);
            selection.append(1);
        }

        if (!TextUtils.isEmpty(province)) {
            if (selection.length() > 0) {
                selection.append(AND);
            }

            selection.append(MuseumContract.PROVINCE);
            selection.append(EQUAL_QUOTE);
            selection.append(province);
            selection.append(QUOTE_CLOSE);
        }

        if (!TextUtils.isEmpty(category)) {
            if (selection.length() > 0) {
                selection.append(AND);
            }
            selection.append(MuseumContract.CATEGORY);
            selection.append(EQUAL_QUOTE);
            selection.append(category);
            selection.append(QUOTE_CLOSE);
        }

        return selection;
    }

    public void saveIntoSharedPreferences(SharedPreferences.Editor editor) {
        editor.putString("filter_province", province);
        editor.putString("filter_category", category);
        editor.putBoolean("filter_free", onlyFree);
        editor.commit();
    }

    public static Filter fromSharedPreferences(SharedPreferences s) {

        String province = s.getString("filter_province", null);
        String category = s.getString("filter_category", null);
        boolean free = s.getBoolean("filter_free", false);

        SharedPreferences.Editor editor = s.edit();
        editor.putString("filter_province", null);
        editor.putString("filter_category", null);
        editor.putBoolean("filter_free", false);

        editor.commit();

        Filter f = new Filter(province, category, free);
        return f;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((category == null) ? 0 : category.hashCode());
        result = prime * result + (onlyFree ? 1231 : 1237);
        result = prime * result + ((province == null) ? 0 : province.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Filter other = (Filter) obj;
        if (category == null) {
            if (other.category != null)
                return false;
        } else if (!category.equals(other.category))
            return false;
        if (onlyFree != other.onlyFree)
            return false;
        if (province == null) {
            if (other.province != null)
                return false;
        } else if (!province.equals(other.province))
            return false;
        return true;
    }

}
