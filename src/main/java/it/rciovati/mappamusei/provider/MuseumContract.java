/*
 * Copyright 2012 Riccardo Ciovati
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package it.rciovati.mappamusei.provider;

import android.net.Uri;

public class MuseumContract {

    public static class Provinces {

        public static final String _ID = "_id";

        public static final String NAME = "name";

        public static final Uri CONTENT_URI = Uri.parse("content://" + MuseumsProvider.AUTHORITY
                + "/provinces");
    }

    public static class Categories {

        public static final String _ID = "_id";

        public static final String NAME = "name";

        public static final Uri CONTENT_URI = Uri.parse("content://" + MuseumsProvider.AUTHORITY
                + "/categories");
    }

    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.rciovati.mappamusei.museums";

    public static final Uri CONTENT_URI = Uri.parse("content://" + MuseumsProvider.AUTHORITY
            + "/museums");

    public static final String _ID = "_id";

    public static final String NAME = "name";

    public static final String LOCATION_NAME = "location_name";

    public static final String PARKING_FOR_DISABLED = "parking_for_disabled";

    public static final String ACCESSIBLE_TO_DISABLED = "accessible_to_disabled";

    public static final String CATEGORY = "type";

    public static final String CITY = "city";

    public static final String PROVINCE = "province";

    public static final String ADDRESS = "address";

    public static final String ZIP = "zip";

    public static final String PHONE = "phone";

    public static final String EMAIL = "mail";

    public static final String WEBSITE = "website";

    public static final String IS_OPEN = "is_open";

    public static final String OPENING_NOTES = "opening_notes";

    public static final String OPENING_HOUR_NOTES = "opening_hour_notes";

    public static final String FREE_ENTRY = "free_entry";

    public static final String OPENING_HOUR_DESCRIPTION = "opening_hour_desc";

    public static final String PARKINGS = "parkings_around";

    public static final String LIBRARY = "library";
    
    public static final String EDUCATIONAL_ACTIVITIES = "educational_activities";
    
    public static final String STUDY_ROOM = "study_room";

    public static final String GUIDED_VISIT = "guided_visit";

    public static final String LATITUDE = "lat";

    public static final String LONGITUDE = "lon";

    public static final String IS_STARRED = "starred";

    public static final String COSLAT = "coslat";

    public static final String SINLAT = "sinlat";

    public static final String COSLNG = "coslng";

    public static final String SINLNG = "sinlng";

    public static final String USER_LAT = "user_lat";

    public static final String USER_LON = "user_lon";

}
