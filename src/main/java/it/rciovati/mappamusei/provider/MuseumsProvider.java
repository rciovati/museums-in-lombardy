/*
 * Copyright 2012 Riccardo Ciovati
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package it.rciovati.mappamusei.provider;

import java.util.Arrays;
import java.util.HashMap;

import it.rciovati.mappamusei.BuildConfig;
import it.rciovati.mappamusei.util.MathUtil;
import android.annotation.TargetApi;
import android.app.SearchManager;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

public class MuseumsProvider extends SQLiteContentProvider {

    public static final String AUTHORITY = "it.rciovati.mappamusei.MuseumProvider";

    private final static String MUSEUM_TABLE_NAME = "museum";

    private static final int MUSEUM_BY_ID = 1;

    private static final int MUSEUMS_LIST = 2;

    private static final int MUSEUMS_LIST_SORTED_BY_DISTANCE = 3;

    private static final int CATEGORIES = 5;

    private static final int PROVINCES = 6;

    private static final int SEARCH = 4;

    private static UriMatcher sUriMatcher;

    private static final String KEY_SEARCH_COLUMN = MuseumContract.NAME;

    private static final HashMap<String, String> SEARCH_SUGGEST_PROJECTION_MAP;

    private static final String[] ALL_PROJECTION = {
            MuseumContract._ID, MuseumContract.NAME, MuseumContract.CITY
    };

    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(AUTHORITY, "museums", MUSEUMS_LIST);
        sUriMatcher
                .addURI(AUTHORITY, "museums-sorted-by-distance", MUSEUMS_LIST_SORTED_BY_DISTANCE);
        sUriMatcher.addURI(AUTHORITY, "museums/#", MUSEUM_BY_ID);

        sUriMatcher.addURI(AUTHORITY, "provinces", PROVINCES);
        sUriMatcher.addURI(AUTHORITY, "categories", CATEGORIES);

        // Search autosuggestion's related stuff

        sUriMatcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY, SEARCH);
        sUriMatcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY + "/*", SEARCH);
        sUriMatcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_SHORTCUT, SEARCH);
        sUriMatcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_SHORTCUT + "/*", SEARCH);

        SEARCH_SUGGEST_PROJECTION_MAP = new HashMap<String, String>();

        SEARCH_SUGGEST_PROJECTION_MAP.put("_id", MuseumContract._ID + " AS _id");
        SEARCH_SUGGEST_PROJECTION_MAP.put(SearchManager.SUGGEST_COLUMN_TEXT_1, KEY_SEARCH_COLUMN
                + " AS " + SearchManager.SUGGEST_COLUMN_TEXT_1);

        // below is for having "city (province)"
        SEARCH_SUGGEST_PROJECTION_MAP.put(SearchManager.SUGGEST_COLUMN_TEXT_2, MuseumContract.CITY
                + " || ' (' || " + MuseumContract.PROVINCE + " || ')' AS "
                + SearchManager.SUGGEST_COLUMN_TEXT_2);
        SEARCH_SUGGEST_PROJECTION_MAP.put(SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID,
                MuseumContract._ID + " AS " + SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID);

    }

    private SQLiteOpenHelper mDbHelper;

    @Override
    protected SQLiteOpenHelper getDatabaseHelper(Context context) {
        mDbHelper = new MuseumSQLiteOpenHeloper(context);
        return mDbHelper;
    }

    @Override
    protected Uri insertInTransaction(Uri uri, ContentValues values) {
        long id = mDbHelper.getWritableDatabase().insert(MUSEUM_TABLE_NAME, null, values);

        if (id > 0) {
            Uri noteUri = ContentUris.withAppendedId(MuseumContract.CONTENT_URI, id);
            getContext().getContentResolver().notifyChange(noteUri, null);
            return noteUri;
        }

        throw new SQLException("Failed to insert row into " + uri);
    }

    @Override
    protected int updateInTransaction(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        return 0;
    }

    @Override
    protected int deleteInTransaction(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sUriMatcher.match(uri);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        switch (uriType) {
            case MUSEUMS_LIST:
                return db.delete(MUSEUM_TABLE_NAME, selection, selectionArgs);

        }

        return 0;
    }

    @Override
    protected void notifyChange(boolean syncToNetwork) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getType(Uri uri) {
        if (sUriMatcher.match(uri) == SEARCH) {
            return SearchManager.SUGGEST_MIME_TYPE;
        }

        return null;
    }

    @TargetApi(9)
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
            String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(MUSEUM_TABLE_NAME);

        if (BuildConfig.DEBUG) {
            Log.d("MuseumsProvider uri: ", uri.toString());
        }

        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        int uriType = sUriMatcher.match(uri);
        switch (uriType) {

            case CATEGORIES:
                projection = new String[] {
                        MuseumContract.CATEGORY + " AS " + MuseumContract.Categories.NAME,
                        "COUNT(" + MuseumContract._ID + ") AS " + MuseumContract.Categories._ID
                };

                queryBuilder.setDistinct(true);
                sortOrder = MuseumContract.CATEGORY + " ASC";

                return db.query(MUSEUM_TABLE_NAME, projection, null, null,
                        MuseumContract.Categories.NAME, null, sortOrder);
            case PROVINCES:
                projection = new String[] {
                        MuseumContract.PROVINCE + " AS " + MuseumContract.Provinces.NAME,
                        "COUNT(" + MuseumContract._ID + ") AS " + MuseumContract.Provinces._ID
                };

                queryBuilder.setDistinct(true);
                sortOrder = MuseumContract.PROVINCE + " ASC";

                return db.query(MUSEUM_TABLE_NAME, projection, null, null,
                        MuseumContract.Provinces.NAME, null, sortOrder);
            case SEARCH:
                String query = uri.getPathSegments().get(1).toLowerCase();
                queryBuilder.appendWhere(KEY_SEARCH_COLUMN + " LIKE \"%" + query + "%\"");
                queryBuilder.setProjectionMap(SEARCH_SUGGEST_PROJECTION_MAP);
                break;
            case MUSEUMS_LIST:
                break;
            case MUSEUMS_LIST_SORTED_BY_DISTANCE:
                String userLat = uri.getQueryParameter(MuseumContract.USER_LAT);
                String userLon = uri.getQueryParameter(MuseumContract.USER_LON);

                String distanceSelection = null;

                if (userLat != null && userLon != null) {
                    double userLatitude = Double.parseDouble(userLat);
                    double userLongitude = Double.parseDouble(userLon);
                    distanceSelection = buildDistanceQuery(userLatitude, userLongitude);

                    if (projection == null) {
                        projection = ALL_PROJECTION;
                    }

                    projection = Arrays.copyOf(projection, projection.length + 1);
                    projection[projection.length - 1] = distanceSelection;

                    sortOrder = "total DESC";
                }

                break;
            case MUSEUM_BY_ID:
                queryBuilder.appendWhere(MuseumContract._ID + "=" + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        Cursor cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null,
                sortOrder);

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    // See http://stackoverflow.com/a/9536914/321354
    public static String buildDistanceQuery(double latitude, double longitude) {
        final double coslat = Math.cos(MathUtil.deg2rad(latitude));
        final double sinlat = Math.sin(MathUtil.deg2rad(latitude));
        final double coslng = Math.cos(MathUtil.deg2rad(longitude));
        final double sinlng = Math.sin(MathUtil.deg2rad(longitude));

        return "(" + coslat + "*" + MuseumContract.COSLAT + "*(" + MuseumContract.COSLNG + "*"
                + coslng + "+" + MuseumContract.SINLNG + "*" + sinlng + ")+" + sinlat + "*"
                + MuseumContract.SINLAT + ") AS total";
    }

    class MuseumSQLiteOpenHeloper extends SQLiteOpenHelper {

        private static final String FILE_NAME = "database.db";

        private static final int DB_VERSION = 1;

        private final static String MUSEUM_TABLE_CREATE = "CREATE TABLE " + MUSEUM_TABLE_NAME
                + "( " + MuseumContract._ID + " integer primary key autoincrement, "
                + MuseumContract.NAME + " text not null, " + MuseumContract.LOCATION_NAME
                + " text not null, " + MuseumContract.LATITUDE + " real not null, "
                + MuseumContract.LONGITUDE + " real not null, "
                + MuseumContract.PARKING_FOR_DISABLED + " integer, "
                + MuseumContract.ACCESSIBLE_TO_DISABLED + " integer, " + MuseumContract.CATEGORY
                + " text, " + MuseumContract.CITY + " text, " + MuseumContract.PROVINCE + " text, "
                + MuseumContract.ADDRESS + " text, " + MuseumContract.ZIP + " text, "
                + MuseumContract.PHONE + " text, " + MuseumContract.EMAIL + " text, "
                + MuseumContract.WEBSITE + " text, " + MuseumContract.IS_OPEN + " integer, "
                + MuseumContract.FREE_ENTRY + " integer, " + MuseumContract.IS_STARRED
                + " integer DEFAULT 0, " + MuseumContract.OPENING_HOUR_DESCRIPTION + " text, "
                + MuseumContract.OPENING_HOUR_NOTES + " text, " + MuseumContract.PARKINGS
                + " integer, " + MuseumContract.GUIDED_VISIT + " integer, "
                + MuseumContract.STUDY_ROOM + " integer, " + MuseumContract.LIBRARY + " integer, "
                + MuseumContract.EDUCATIONAL_ACTIVITIES + " integer, " + MuseumContract.COSLAT
                + " real, " + MuseumContract.COSLNG + " real, " + MuseumContract.SINLAT + " real, "
                + MuseumContract.SINLNG + " real " + ");";

        private static final String CREATE_NAME_INDEX = "CREATE INDEX name_index ON "
                + MUSEUM_TABLE_NAME + " (" + MuseumContract.NAME + ");";

        public MuseumSQLiteOpenHeloper(Context context) {
            super(context, FILE_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            if (BuildConfig.DEBUG)
                Log.d("create table query", MUSEUM_TABLE_CREATE);
            db.execSQL(MUSEUM_TABLE_CREATE);
            db.execSQL(CREATE_NAME_INDEX);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // Not used, atm.
        }

    }

}
