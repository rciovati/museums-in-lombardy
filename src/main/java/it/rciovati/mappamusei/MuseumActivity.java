/*
 * Copyright 2012 Riccardo Ciovati
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package it.rciovati.mappamusei;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import it.rciovati.mappamusei.events.OnMuseumSelectedListener;
import it.rciovati.mappamusei.events.OnSortTypeChanged;
import it.rciovati.mappamusei.fragments.FilterMuseumsListFragmentDialog;
import it.rciovati.mappamusei.fragments.MapFragment;
import it.rciovati.mappamusei.fragments.MuseumDetailFragment;
import it.rciovati.mappamusei.fragments.MuseumsListFragment;
import it.rciovati.mappamusei.fragments.SortOptionsDialogFragment;
import it.rciovati.mappamusei.util.Filter;
import it.rciovati.mappamusei.util.SortType;

public class MuseumActivity extends BaseActivity implements OnSortTypeChanged,
        OnMuseumSelectedListener {

    private static final String KEY_FILTER = "filter";

    private static final String KEY_DETAIL_FRAGMENT_VISIBLE = "detail_visible";

    private static final String KEY_TAB = "tab";

    private static final String TAG_DETAIL = "detail";

    private static final String TAG_LIST = "list";

    private static final String TAG_MAP = "map";

    public static final String PREFS_NAME = "preferences";

    private boolean mHasTwoPanes = false;

    MapFragment mMapFragment;

    MuseumsListFragment mMuseumListFragment;

    private MuseumDetailFragment mDetailFragment;

    public String TAG = MuseumActivity.class.getCanonicalName();

    private Filter mFilter = Filter.EMPTY;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        boolean setup_done = settings.getBoolean("setup_done", false);

        if (!setup_done) {
            startActivity(new Intent(this, WizardActivity.class));
            finish();
            return;
        }

        mHasTwoPanes = getResources().getBoolean(R.bool.has_two_panes);

        if (!mHasTwoPanes) {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

            ActionBar.Tab tab = actionBar
                    .newTab()
                    .setText(R.string.title_section_list)
                    .setTabListener(
                            new TabListener<MuseumsListFragment>(this, TAG_LIST,
                                    MuseumsListFragment.class));
            actionBar.addTab(tab);

            ActionBar.Tab tab1 = actionBar.newTab().setText(R.string.title_section_map)
                    .setTabListener(new TabListener<MapFragment>(this, TAG_MAP, MapFragment.class));
            actionBar.addTab(tab1);
        } else {
            setContentView(R.layout.main_layout);
            mMapFragment = (MapFragment) getSupportFragmentManager().findFragmentByTag(TAG_MAP);
            mMuseumListFragment = (MuseumsListFragment) getSupportFragmentManager()
                    .findFragmentByTag(TAG_LIST);
            mDetailFragment = (MuseumDetailFragment) getSupportFragmentManager().findFragmentByTag(
                    TAG_DETAIL);

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.hide(mDetailFragment);
            ft.commit();

            mMuseumListFragment.getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }

        mFilter = Filter.fromSharedPreferences(settings);
    }

    @Override
    protected void onPause() {
        if (!mFilter.equals(Filter.EMPTY)) {
            SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
            mFilter.saveIntoSharedPreferences(settings.edit());
        }
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle out) {
        if (!mHasTwoPanes) {
            out.putInt(KEY_TAB, getSupportActionBar().getSelectedNavigationIndex());
        } else {
            if (mDetailFragment != null && mDetailFragment.isVisible()) {
                out.putBoolean(KEY_DETAIL_FRAGMENT_VISIBLE, true);
            }
        }

        if (mFilter != Filter.EMPTY)
            out.putSerializable(KEY_FILTER, mFilter);

        super.onSaveInstanceState(out);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {

        if (savedInstanceState == null) {
            return;
        }

        if (savedInstanceState.containsKey(KEY_TAB))
            getSupportActionBar().setSelectedNavigationItem(savedInstanceState.getInt(KEY_TAB));

        if (savedInstanceState.containsKey(KEY_DETAIL_FRAGMENT_VISIBLE)) {

            boolean showDetailFragment = savedInstanceState.getBoolean(KEY_DETAIL_FRAGMENT_VISIBLE);
            if (mHasTwoPanes && showDetailFragment && mDetailFragment != null
                    && mDetailFragment.isHidden()) {

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.show(mDetailFragment);
                ft.commit();

            }
        }

        if (savedInstanceState.containsKey(KEY_FILTER)) {
            mFilter = (Filter) savedInstanceState.getSerializable(KEY_FILTER);
        }

        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_museum, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_search:
                return true;
            case R.id.menu_filter:
                FilterMuseumsListFragmentDialog dialog = FilterMuseumsListFragmentDialog
                        .newInstance();
                Bundle b = new Bundle();
                b.putSerializable(KEY_FILTER, mFilter);
                dialog.setArguments(b);
                dialog.show(getSupportFragmentManager(), "dialog");
                return true;
            case R.id.menu_about:
                startActivity(new Intent(this, AboutActivity.class));
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public class TabListener<T extends Fragment> implements ActionBar.TabListener {

        private Fragment mFragment;

        private final Activity mActivity;

        private final String mTag;

        private final Class<T> mClass;

        public TabListener(Activity activity, String tag, Class<T> clz) {
            mActivity = activity;
            mTag = tag;
            mClass = clz;
            mFragment = getSupportFragmentManager().findFragmentByTag(mTag);
        }

        public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
            if (mFragment == null) {
                mFragment = Fragment.instantiate(mActivity, mClass.getName());
                ft.add(android.R.id.content, mFragment, mTag);
            } else {
                if (mFragment.isDetached()) {
                    ft.attach(mFragment);
                } else {
                    Log.d(TAG, "onTabSelected fragment already attached " + mTag);
                }
            }
        }

        public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
            if (mFragment != null) {
                ft.detach(mFragment);
            }
        }

        public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
        }
    }

    public void displayMuseumsSortTypeDialog(SortType actualSortType) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        SortOptionsDialogFragment dialog = SortOptionsDialogFragment.newInstance();
        Bundle b = new Bundle();
        b.putSerializable("s", actualSortType);
        dialog.setArguments(b);
        dialog.show(ft, "dialog");
    }

    @Override
    public void onSortTypeChanged(SortType type) {
        MuseumsListFragment listFragment = (MuseumsListFragment) getSupportFragmentManager()
                .findFragmentByTag(TAG_LIST);
        listFragment.setSortType(type);
    }

    @Override
    public void onMuseumSelected(Fragment source, View sourceView, Uri selectedUri) {
        if (mHasTwoPanes) {

            if (source instanceof MuseumsListFragment)
                mMapFragment.setFocusOnMuseum(selectedUri);
            else if (source instanceof MapFragment)
                mMuseumListFragment.selectMuseum(selectedUri);

            mDetailFragment.showMuseum(selectedUri);

            if (!mDetailFragment.isVisible()) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out,
                        android.R.anim.fade_in, android.R.anim.fade_out);
                ft.show(mDetailFragment);
                ft.addToBackStack(null);
                ft.commit();
                getSupportFragmentManager().executePendingTransactions();
            }

        } else {

            Intent i = new Intent(this, MuseumDetailActivity.class);
            i.putExtra(MuseumDetailActivity.KEY_MUSEUM, selectedUri);

            if (sourceView != null && Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {

                ActivityOptionsCompat options = ActivityOptionsCompat.makeScaleUpAnimation(sourceView, (int) sourceView.getX(), (int) sourceView.getY(), sourceView.getWidth(), sourceView.getHeight());
                ActivityCompat.startActivity(this, i, options.toBundle());

            } else {

                startActivity(i);
            }
        }
    }

    public void hideDetailFragment() {
        getSupportFragmentManager().popBackStack();

        Log.d("hideDetailFragment", "count: "
                + getSupportFragmentManager().getBackStackEntryCount());
    }

    public void filter(Filter filter) {

        if (!mFilter.equals(filter)) {
            if (mHasTwoPanes) {
                hideDetailFragment();
                mMapFragment.filter(filter);
                mMuseumListFragment.filter(filter);
            } else {
                MapFragment map = (MapFragment) getSupportFragmentManager().findFragmentByTag(
                        TAG_MAP);

                if (map != null) {
                    map.filter(filter);
                }

                MuseumsListFragment list = (MuseumsListFragment) getSupportFragmentManager()
                        .findFragmentByTag(TAG_LIST);

                if (list != null) {
                    list.filter(filter);
                }
            }

            mFilter = filter;

            MappaMuseiApplication app = (MappaMuseiApplication) getApplication();
            app.setMuseumsFilter(filter);
        }
    }

}
