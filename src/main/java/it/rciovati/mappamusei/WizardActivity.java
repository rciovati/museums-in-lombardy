/*
 * Copyright 2012 Riccardo Ciovati
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package it.rciovati.mappamusei;

import it.rciovati.mappamusei.service.DataFetcherService;
import it.rciovati.mappamusei.util.CustomResultReceiver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher.ViewFactory;

public class WizardActivity extends BaseActivity implements CustomResultReceiver.Receiver,
        OnClickListener, ViewFactory {

    CustomResultReceiver mResultReceiver;

    Button mContinueButton;

    TextSwitcher mTextSwitcher;

    ProgressBar mProgressBar;

    private final static int STATE_DOWNLOAD_IN_PROGRESS = 1;

    private final static int STATE_DOWNLOAD_FAILED = 2;

    private final static int STATE_INITIAL = 3;

    private int mCurrentState = STATE_INITIAL;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_wizard);

        mResultReceiver = new CustomResultReceiver(new Handler());
        mResultReceiver.setReceiver(this);

        mContinueButton = (Button) findViewById(R.id.continue_button);
        mTextSwitcher = (TextSwitcher) findViewById(R.id.text_switcher);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        mContinueButton.setOnClickListener(this);
        mTextSwitcher.setFactory(this);

        String a = getString(R.string.download_summary);

        mTextSwitcher.setCurrentText(a);

        // Intent i = new Intent(this, DataFetcherService.class);
        // i.putExtra(DataFetcherService.EXTRA_RESULT_RECEIVER,
        // mResultReceiver);
        // startService(i);

        // Uri u = MuseumContract.CONTENT_URI.buildUpon()
        // .appendQueryParameter(MuseumContract.USER_LAT, "45.47870")
        // .appendQueryParameter(MuseumContract.USER_LON, "9.14567")
        // .build();
        //
        // Cursor c = getContentResolver().query(u, null, null, null, null);
        //
        // Log.d("main", DatabaseUtils.dumpCursorToString(c));

    }

    private void changeState(int nextState) {
        switch (nextState) {
            case STATE_DOWNLOAD_IN_PROGRESS:
                mProgressBar.setVisibility(View.VISIBLE);
                mProgressBar.setIndeterminate(true);
                mContinueButton.setEnabled(false);
                mTextSwitcher.setText(getString(R.string.download_in_progress));
                break;
            case STATE_DOWNLOAD_FAILED:
                mProgressBar.setVisibility(View.GONE);
                mContinueButton.setText(R.string.retry_string);
                mContinueButton.setEnabled(true);
                mTextSwitcher.setText(Html.fromHtml(getString(R.string.download_failed)));
                break;
        }

        mCurrentState = nextState;
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        if (resultCode == DataFetcherService.RESULT_DATA_FETCHED) {

            SharedPreferences settings = getSharedPreferences(MuseumActivity.PREFS_NAME, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("setup_done", true);
            editor.commit();

            Intent i = new Intent(this, MuseumActivity.class);
            startActivity(i);
            finish();
        } else {
            changeState(STATE_DOWNLOAD_FAILED);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.continue_button) {
            changeState(STATE_DOWNLOAD_IN_PROGRESS);
            Intent i = new Intent(this, DataFetcherService.class);
            i.putExtra(DataFetcherService.EXTRA_RESULT_RECEIVER, mResultReceiver);
            startService(i);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("state", mCurrentState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        int state = savedInstanceState.getInt("state");
        if (state != STATE_INITIAL) {
            changeState(state);
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View makeView() {
        TextView t = new TextView(this);
        t.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
        t.setTextAppearance(this, R.style.welcomeText);
        return t;
    }

}
