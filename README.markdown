#Museums in Lombardy

Available on [Google Play Store](https://play.google.com/store/apps/details?id=it.rciovati.mappamusei)

## Credits

Author: Riccardo Ciovati

Help on User Interface & User Experience: Taylor Ling

##License

    Copyright 2012 Riccardo Ciovati

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.